package com.example;

import org.kie.api.builder.KieScanner;
import org.kie.api.runtime.KieContainer;

public class KJAR {

	private String groupId;
	private String artifactId;
	private String version;
	private Integer scanningInterval;
	private KieContainer container;
	private KieScanner scanner;


	public KJAR() {
	}

	public KJAR(String groupId, String artifactId, String version, Integer scanningInterval, KieContainer container, KieScanner scanner) {
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.version = version;
		this.scanningInterval = scanningInterval;
		this.container = container;
		this.scanner = scanner;
	}

	public String getGroupId() {
		return this.groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getArtifactId() {
		return this.artifactId;
	}

	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getScanningInterval() {
		return this.scanningInterval;
	}

	public void setScanningInterval(Integer scanningInterval) {
		this.scanningInterval = scanningInterval;
	}

	public KieContainer getContainer() {
		return this.container;
	}

	public void setContainer(KieContainer container) {
		this.container = container;
	}

	public KieScanner getScanner() {
		return this.scanner;
	}

	public void setScanner(KieScanner scanner) {
		this.scanner = scanner;
	}

	public KJAR groupId(String groupId) {
		setGroupId(groupId);
		return this;
	}

	public KJAR artifactId(String artifactId) {
		setArtifactId(artifactId);
		return this;
	}

	public KJAR version(String version) {
		setVersion(version);
		return this;
	}

	public KJAR scanningInterval(Integer scanningInterval) {
		setScanningInterval(scanningInterval);
		return this;
	}

	public KJAR container(KieContainer container) {
		setContainer(container);
		return this;
	}

	public KJAR scanner(KieScanner scanner) {
		setScanner(scanner);
		return this;
	}
	@Override
	public String toString() {
		return "KJAR{" +
			" groupId='" + getGroupId() + "'" +
			", artifactId='" + getArtifactId() + "'" +
			", version='" + getVersion() + "'" +
			", scanningInterval='" + getScanningInterval() + "'" +
			", container='" + getContainer() + "'" +
			", scanner='" + getScanner() + "'" +
			"}";
	}

}
