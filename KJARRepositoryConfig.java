package com.example;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieScanner;
import org.kie.api.builder.ReleaseId;
import org.kie.api.builder.Message.Level;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ConfigurationProperties(prefix = "rules")
// @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class KJARRepositoryConfig {

	private static final String kieMavenSettingsProp = "kie.maven.settings.custom";
	private static final String kieBaseName = "rules-kie-base";

	private String mavenSettings;

	private static final Logger LOGGER = LoggerFactory.getLogger(KJARRepositoryConfig.class);

	@Bean(name = "kjarRepository")
	public KJARRepositoryConfig getRepository() {

		if (StringUtils.isNotEmpty(mavenSettings)) {
			LOGGER.info("Setting '{}' property : {} ", kieMavenSettingsProp, mavenSettings);
			System.setProperty(kieMavenSettingsProp, mavenSettings);
		}

		return this;

	}

	public void initKJAR(KJAR kjar) {

		KieServices kieServices = KieServices.Factory.get();
		ReleaseId releaseId = kieServices.newReleaseId(kjar.getGroupId(), kjar.getArtifactId(), kjar.getVersion());
		kjar.setContainer(kieServices.newKieContainer(releaseId));
		
		KieScanner kScanner = KieServices.get().newKieScanner(kjar.getContainer());
		kScanner.addListener(new CountryKieBaseListener(kjar));
		kScanner.start(kjar.getScanningInterval());
		kjar.setScanner(kScanner);
	}

	public KieSession createNewKieSession(container) {
		final long executionStart = System.currentTimeMillis();
		final KieSession kieSession = container.getKieBase(kieBaseName).newKieSession();
		LOGGER.debug("{} kie session created in {}ms", kieBaseName, (System.currentTimeMillis() - executionStart));
		return kieSession;
	};

	public void tearDownKieSession(kieSession) {
			LOGGER.info("Dispose of KIE Session with id {}", kieSession.getIdentifier());
			kieSession.dispose();
			kieSession.destroy();
		};
	
	}

}
